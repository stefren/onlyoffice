const request = require('request');
const xmlParser = require('xml2js').parseString;
const convertUrl = "http://117.50.9.218:8090";
const uuid = require('uuid/v4');
const fs = require('fs');

//html转docx
function html2docx(url) {
    return new Promise(function (resolve, reject) {
        var json = {
            "filetype": "html",    //源文件类型
            "key": uuid(),         //识别key
            "outputtype": "docx",  //输出文件类型
            "title": "footitle",   //标题
            "url": url             //源文件链接（可访问）
        };
        request({
            url: convertUrl+"/ConvertService.ashx",
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            json: json,
            timeout: 10000
        }, function (err, resp, body) {
            if (err) console.log(err);
            console.log('callback',body);
            resolve(body);
        })
    })
}

//下载转化好的docx
async function downloadDocx(url) {
    let callback = await html2docx(url);
    request(callback.fileUrl).pipe(fs.createWriteStream('docs/baidu.docx'));
    return null
}

//下载百度首页
downloadDocx('https://www.baidu.com/index.html');
